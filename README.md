### Autor: Magdalena Tokarczyk ###

## zad.1 OutOfMemoryError - na branchu outOfMemoryError

Uruchomienie programu:
 1. Przejście na odpowiedniego brancha: git checkout outOfMemoryError
 2. Komenda: mvn install exec:java
 
Po skompilowaniu oraz uruchomieniu programu jego praca zakończy się wyjątkiem OutOfMemoryError.
Jest to spowodowane tym, że w czasie pracy tworzona jest instancja klasy MyOutOfMemoryError i wywoływana jej metoda causeError.
Metoda ta, w niekończącej się pętli while (warunek 1<2 jest zawsze spełniony), zapisuje na liście (ArrayList) nowo tworzone listy typu double długości 1.000.000.000.
Jedna taka lista przekracza swoim rozmiarem dopuszczalny rozmiar sterty, ale na wypadek innych ustawień JVM dodaję ich więcej w pętli.
Odśmiecacz nie może usunąć żadnej z dodawanych list, bo cały zmienna referencyjna "list" się do nich odnosi.

## zad.2 JSON Converter - na branchu JSON-converter
Uruchomienie programu: 
1. Przejście na odpowiedniego brancha: git checkout JSON-converter
2. Komenda mvn install exec:java -Dexec.args="<i>ścieżka do lokalizacji zapisu wygenerowanych plików</i>"

Przykład poprawnej komendy: mvn install exec:java -Dexec.args="C:/Users/Magdalena/Desktop/wyniki".

W wyniku działania programu zostaną wygenerowane 2 pliki: "mojMapper.json" (wynik pracy mappera, którego napisałam) oraz "wzor.json" (wynik pracy klasy ObjectMapper).

Mapper, który napisałam obsługuje pola publiczne, prywatne, statyczne, pola, których wartościami są typy proste oraz obiekty i listy (List<Object>), pola o wartości null.

## zad.3 GC-performance - na branchu GC-performance
Uruchomienie programu:
1. Przejście na odpowiedniego brancha: git checkout GC-performance
2. Komenda mvn install exec:java -Dexec.args="<i>ścieżka do lokalizacji zapisu wygenerowanych plików</i>"

Przykład poprawnej komendy: mvn install exec:java -Dexec.args="C:/Users/Magdalena/Desktop/wyniki".

Różne testowane warianty zadania zostały zaszyte w kodzie. Uruchomienie testu innej funkcji wymaga ręcznie wprowadzonych zmian. Użytkownik jedynie podaje lokalizację zapisu pliku wynikowego (z czasami obliczeń).
Sprawdzam czas utworzenia 1.000.000 instancji obiektu klasy Student na 4 sposoby: 
1. Jedyną składową klasy Student jest jest tablica typu int o rozmiarze 1024 (basicExperiment).
2. Jedyną składową klasy Student jest tablica typu int o wylosowanym rozmiarze z zakresu [0,1023] (randomExperiment).
3. Jedyną składową klasy Student jest jest tablica typu int o rozmiarze 1024 (basicExperiment), dwa wątki tworzą po 500.000 instancji każdy. (basicMultihreadExperiment).
4. Jedyną składową klasy Student jest tablica typu int o wylosowanym rozmiarze z zakresu [0,1023], dwa wątki tworzą po 500.000 instancji każdy. (randomMultihreadExperiment).

Wyniki załączam w folderze projektu *resources* - przedstawiłam je w arkuszach programu Excel, na wszelki wypadek załączyłam print screeny tych arkuszy.
Każdy z testów (basciExperiment, randomExperiment, basicMultithreadExperiment, randomMultithreadExperiment) przeprowadziłam z ustawieniami Garbage Collectora:
1. Parallel Old 128MB.
2. Parallel Old 512MB.
3. Parallel Old 1GB.
4. Concurrent Mark and Sweep 128MB.
5. Concurrent Mark and Sweep 512MB.
6. Concurrent Mark and Sweep 1GB.
7. Garbage First 128MB.
8. Garbage First 512MB.
8. Garbage First 1GB.

Dla każdego ustawienia JVM wykonuję 100 testów i obliczam średnią, maksimum oraz minimum z otrzymanych czasów. Pomijam wynik pierwszej iteracji algorytmu (jako zaburzony, zawsze jest duży wyższy).
Wyniki zestawiłam według wariantów algorytmu, ponieważ ustawienia JVM również w praktyce powinno się wybierać pod konkretne aplikacje.
Dla każdego algorytmu zaznaczyłam na wykresie i w tabeli najniższe wartości (najniższa średnia, najniższe maksimum i najniższe minimum), chociaż preferuję (po odrzuceniu zaburzonych pierwszych wyników) uznać za najlepsze ustawienie JVM takie, dla którego maksymalny czas pracy jest najkrótszy, ponieważ dla aplikacji te najgorsze przypadki mają największe znaczenie.

Zatem najlepsze uzyskane ustawienia dla poszczególnych wariantów algorytmu:
 1. Basic Experiment -> Parallel Old 128MB
 2. Random Experiment -> Concurrent Mark and Sweep 128 MB.
 3. Basic Multithread Experiment -> Parallel Old 512 MB.
 4. Random Multithread Experiment -> Concurrent Mark & Sweep 512MB.
 
 
 Testy przeprowadzone na systemie operacyjnym *Windows 7 Professional*

## zad.4 SimpleDateFormat - na branchu simpleDateFormat

Uruchomienie programu:
 1. Przejście na odpowiedniego brancha: git checkout simpleDateFormat
 2. Komenda: mvn install exec:java

W pierwszej części algorytmu uruchamianych jest 15 wątków, które korzystają z mojej bezpiecznej klasy SafeDateFormat. Klasa ta ma składową typu ThreadLocal<SimpleDateFormat> (o nazwie safeDateFormat), zatem każdy wątek dostaje do użytku swoją własną kopię tej składowej.
SafeDateFormat ma metodę parse(), która wywołuje metodę parse() składowej safeDateFormat. 
Ta bezpieczna metoda parse() jest uruchamiana 15 razy (przez 15 wątków) dla daty 22/03/2017. Otrzymany wynik jest wypisywany.
Analogiczna operacja jest wykonana dla 15 wąków oraz klasy SimpleDateFormat.
W rezultacie bezpieczne metody działają zawsze (każdy wątek korzysta ze swojej bezpiecznej składowej, więc nie ma powodu by było inaczej), a niebezpieczna meotda często nie działa. W lepszym przypadku (łatwo wykryć błąd) rzucany jest wyjątek spowodowany próbą równoczesnego odczytu tego samego miejsca w pamięci. W gorszym przypadku (trudno wykryć błąd) nie jest rzucany wyjątek, ale niektóre wypisywane daty są niepoprawne. Czasem niebezpieczna metoda działa.
W folderze resources znajdują się printscreeny z przebiegu programu.
1.png -> niepoprawne dane
2.png -> program wykonał się bezbłędnie
3.png -> program rzucił wyjątek


## Instrumentacja - zadanie pierwsze (CL w aplikacji webowej)

Rozwiązanie tego zadania zostało umieszczone w osobnym projekcie - https://taryfikator@gitlab.com/taryfikator/CL.git na branchu master.

## Instrumentacja - zadanie drugie (klasy pojawiające się podczas pracy serwera) - na branchu zad52

Udało mi już się podpiąc agenta pod aplikację webową. Podobnie jak w zadaniu 6, nie umiem uruchomić programu z konsoli (są błędy), których w IntelliJu nie ma. W folderze resources załączam zrzuty ekranu z przebiegu programu. Po otworzeniu strony http://localhost:4567/wait5sec ładowanych jest więcej klas.

## Javaassist - zadanie z zajęć numer 7 - na branchu zad6

Udało mi się napisać i uruchomić aplikację w IntelliJu. Nie udało mi się jej uruchomić z linii komend. W folderze resources zamieściłam zrzuty ekranu z okna przeglądarki oraz konsoli aplikacyjnej.

## Midterm project - na branchu jsonassist

Konwerter obsługuje: pola publiczne, pola niepubliczne z getterami, typy proste oraz obiekty.
Składowe będące obiektami mogą być zagnieżdżane (przykład - klasa TwoLayersOfClasses), 
ale nie obsługuję klas, które zawierają same siebie jako składowe.
Obsługiwane pola mogą występować w różnych kombinacjach, co próbuję pokazać w testach.

Aplikacja bezpieczna wielowątkowo - użyłam ConcurrentHashMap.
Testy zwracają porównanie czasów oraz wyniki generowane przez GSONa i testowany konwerter.
W folderze resources w pliku 1.png znajduje się zrzut ekranu z wynikami działania aplikacji.

## Streams - na branchu streams
Uruchomienie: mvn clean install

## JMX - na branchu JMX
#W folderze resources załączam ilustrację przeprowadzonych eksperymentów.
# Opis:
Początkowo uruchamiam aplikację ze składową "power" obiektu "powerService" ustawioną na 1 (ilustracja 1). 
Uruchamiam JConsole, włączam subskrypcję notyfikacji (ilustracja 2).
Zmieniam wartość składowej "power" na 2 (ilustracja 3). 
Odbieram notyfikację i sprawdzam, że zmieniło się działanie aplikacji (ilustracja 4).
Dla potęg z innych zakresów (niskie, średnie, wysokie) przychodzą odpowiednie powiadomienia i zmienia się wykładnik (sprawdziłam nie ilustrowałam).


## Javassist vs ASM - na branchu javassistVsASM

Napisałam aplikację, która ładuje po 5000 razy dwie klasy różniące się jedynie nazwą.
Podczas ładowania jednej z nich Javassist zmienia metodę doSomething(), dodając wypisanie "Just called doSomething method!".
Podczas ładowania drugiej taką samą operację przeprowadza ASM.
Czas ładowania tych klas jest mierzony i wypisywany. Do tego wypisywane jest o ile procent szybsze jest jedno narzędzie od drugiego.

Dodatkowo prezentowane jest działanie obu zmienionych metod.

Uruchomienie programu:
Komenda: mvn install exec:javagit 


## własny classLoader - na branchu customClassLoader

#W folderze resources załączam ilustrację przeprowadzonych eksperymentów.

# Opis:
W metodzie main jest funkcjonalność wysyłania plików z folderu src/main/resources/public na serwer http://localhost:4567/<b>nazwa pliku</b>.
Za każdym razem gdy plik zostanie uaktualniony w src/main/resources/public, zostanie również uaktualniony na serwerze.

Eksperyment:
1. Początkowa wersja klasy ładowanej przez mój własny classLoader to v0. (ilustracje 1 oraz 2).

2. Nadpisuję plik na serwerze ServiceImpl.class (ilustracje 3 i 4) poprzez małą zmianę na pliku w src/main/resources.

3. Bezpośrednio po nadpisaniu ładowana klasa zmienia swoją wersję (ilustracja 5 oraz 6).

